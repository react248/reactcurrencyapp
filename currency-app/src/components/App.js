import Header from "./Header";
import Exchanger from "./Exchanger";
import "../CSS/app.css";

const App = () => {
  return (
    <div className="app">
      <div className="main-container">
        <Header />
        <Exchanger />
      </div>
    </div>
  );
};

export default App;
