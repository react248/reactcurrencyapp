import "./../CSS/input.css";

const Input = ({ input, preventMinus }) => {
  return (
    <input
      min="0"
      onChange={input}
      id="input"
      placeholder="Value"
      type="number"
      onKeyPress={preventMinus}
    />
  );
};

export default Input;
